#Western Washington College Department Theme#
This is the theme for the WWU Communications department.

##Dependencies##
  * [WWUZEN](https://bitbucket.org/wwuweb/wwuzen)
  * Make sure you meet the dependencies for WWUZEN as well.

##Configuration##
  * [Instructions for utilizing this theme](https://www.wwu.edu/webtech/drupal/dual-theme-configuration.shtml)



###Copyright Statement###

Copyright 2013 Western Washington University Web Technologies Group Licensed under the Educational Community License, Version 2.0 (the "License") you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.osedu.org/licenses/ECL-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied. See the License for the specific language governing permissions and limitations under the License.
